import React from "react"
import "./Pagination.css"

class Pagination extends React.Component {
	render() {
		return (
			<div className="pagination">
				<div className="first btn" onClick={this.props.prevPage}>{'<'}</div>
				<span className="count">{this.props.pageNo}</span>
				<div className="last btn" onClick={this.props.nextPage}> {'>'}</div>
			</div>
		)
	}
}

export default Pagination