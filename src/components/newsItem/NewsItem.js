import React from "react"
import "./NewsItem.css"

class NewsItem extends React.Component {
    constructor(props) {
        super(props)
        this.time = new Date(parseInt(this.props.time) * 1000)
    }

    render() {

        return (
            <div className="title-container">
                <p className="title">{this.props.title}</p>
                <p className="details">
                    {`${this.props.score} points |
                    by ${this.props.by} | 
                     ${this.props.kids === undefined ? 0 : this.props.kids.length} comments | 
                    posted on ${this.time.toDateString()}`}
                </p>
                <span className="source"> Source: </span>{this.props.url ? <a href={this.props.url} className="url">({this.props.url.substr(0, 35)}...)</a> : <span className="url">None</span>}
            </div>
        )
    }
}

export default NewsItem