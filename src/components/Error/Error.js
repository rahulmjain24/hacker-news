import React from 'react'
import "./Error.css"

class Error extends React.Component {
    render() { 
        return (
            <h1 className='error'>{this.props.children}</h1>
        )
    }
}
 
export default Error