import React from "react"
import "./NavBar.css"

class NavBar extends React.Component {

    render() {
        return (
            <nav className="nav-container">
                <div className="nav">
                    <div className="logo">Hacker News</div>
                    <form action="submit" className="search-form" onSubmit={(e) => {
                        e.preventDefault()
                    }}>
                        <div className="search">
                            <input type="text" className="search-input" placeholder="Search" onChange={(event) => {
                                this.props.check(event)
                            }}/>
                        </div>
                    </form>
                </div>
            </nav>
        )
    }
}

export default NavBar