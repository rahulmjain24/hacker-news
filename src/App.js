import React from 'react'
import './App.css'
import NewsItem from './components/newsItem/NewsItem'
import NavBar from './components/NavBar/NavBar'
import Pagination from './components/Pagination/Pagination'
import Loader from './components/Loader/Loader'
import Error from './components/Error/Error'

class App extends React.Component {
	constructor(props) {
		console.log('constructor')
		super(props)
		this.state = {
			news: [],
			pageNo: 1,
			newsData: [],
			searchArr: [],
			query: '',
			isUpdating: false,
			error: false
		}
		this.TOP_STORIES = 'https://hacker-news.firebaseio.com/v0/topstories.json'
		this.BEST_STORIES = 'https://hacker-news.firebaseio.com/v0/beststories.json'
		this.NEW_STORIES = 'https://hacker-news.firebaseio.com/v0/newstories.json'

		this.ITEM_URL = 'https://hacker-news.firebaseio.com/v0/item/'

		this.ITEMS_PER_PAGE = 15
	}

	componentDidMount() {
		console.log('componend did mount')
		fetch(this.TOP_STORIES)
			.then((response) => {
				return response.json()
			})
			.then((data) => {
				// console.log(data)
				this.setState({ news: data })
				return data.slice((this.state.pageNo - 1) * this.ITEMS_PER_PAGE, this.state.pageNo * this.ITEMS_PER_PAGE)
				// return data
			})
			.then((slicedData) => {
				return Promise.all(slicedData.map((itemID) => {
					return fetch(this.ITEM_URL + itemID + '.json').then((item) => {
						return item.json()
					})
					.then((item) => {
						if(item.error) {
							throw new Error(item.error)
						}
						return item
					})
					.catch((err) => {
						this.setState({error: true})
					})
				}))
			})
			.then((newsItems) => {
				console.log(newsItems)
				this.setState({ newsData: newsItems })
			})
			.catch((err) => {
				console.error(err)
				this.setState({error: true})
			})
	}

	componentDidUpdate(prevProps, prevState) {
		if (prevState.pageNo !== this.state.pageNo) {
			console.log('componend did update')
			const slicedIds = this.state.news.slice((this.state.pageNo - 1) * this.ITEMS_PER_PAGE, this.state.pageNo * this.ITEMS_PER_PAGE)
			Promise.all(slicedIds.map((itemID) => {
					return fetch(this.ITEM_URL + itemID + '.json').then((item) => {
						return item.json()
					})
					.then((item) => {
						if(item.error) {
							throw new Error(item.error)
						}
						return item
					})
					.catch((err) => {
						this.setState({error: true})
					})
				}))
				.then((newsItems) => {
					this.setState({ newsData: newsItems, isUpdating: false })
				})
				.catch((err) => {
					console.error(err)
					this.setState({error: true})
				})
		}
	}

	check = (event) => {
		const query = event.target.value.toLowerCase().trim()
		if (query !== "" && this.state.newsData.length > 0) {

			this.setState({
				searchArr: this.state.newsData.filter((news) => {
					if (news.url) {
						return news.title.toLowerCase().includes(query) || news.url.toLowerCase().includes(query) || news.by.toLowerCase().includes(query)
					}
					return news.title.toLowerCase().includes(query) || news.by.toLowerCase().includes(query)
				}),
				query: query
			})
		} else {
			this.setState({ searchArr: [], query: '' })
		}
	}

	prevPage = () => {
		if (this.state.pageNo > 1) {
			this.setState({ pageNo: this.state.pageNo - 1, isUpdating: true })
		}
	}

	nextPage = () => {
		if (this.state.pageNo <= this.state.news.length / this.ITEMS_PER_PAGE) {
			this.setState({ pageNo: this.state.pageNo + 1, isUpdating: true })
		}
	}

	render() {
		console.log('render')
		return (
			<div className="App">
				<NavBar check={this.check} />
				<div className="news-container">
					{this.state.error ? <Error>Something went wrong...</Error> : 
					(this.state.newsData.length === 0 || this.state.isUpdating ? 
					<Loader /> 
					: 
					this.state.query === '' ?
						<>
							{(this.state.newsData.map((newsItem) => {
								return (
									<NewsItem
										{...newsItem}
										key={newsItem.id}
									/>
								)
							}))}
							<Pagination
								prevPage={this.prevPage}
								nextPage={this.nextPage}
								pageNo={this.state.pageNo}
							/>
						</>
						:
						<>
							{(this.state.searchArr.map((newsItem) => {
									return (
										<NewsItem
											{...newsItem}
											key={newsItem.id}
										/>
									)
								}))}
							{this.state.searchArr.length === 0 && <Error>No results found!</Error>}
						</>
					)
				}
				</div>
			</div>
		)
	}
}

export default App
